const path = require('path');
module.exports = {
    watchPaths: ['src/**/*.ts'],
    watchers: [
        {
            test: /\.ts$/,
            command: './node_modules/.bin/tsc'
        },
        {
            test: /\.ts$/,
            command: './node_modules/.bin/jest {file}',
            getFile(file) {
                if (/\.test\.ts$/.test(file)) {
                    return file;
                }

                const parsed = path.parse(file);
                return `${parsed.dir}/${parsed.name}.test.ts`;
            }
        }
    ]
};
