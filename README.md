<div align="center">
    <h1>Watcher</h1>
    <p>Cli app to watch files and run commands when they have changed.</p>
</div>
<hr />

## Installation

(Coming Soon)

## Usage

Create a `watcher.config.js` file in the root of your project to define all
of your watchers, once you have defined your watchers run the command.

```sh
watcher watch
```

This will start the watcher and start running commands when you change the
watched files.

If you don't want to create a `watcher.config.js` you can pass in a custom
config location by running the following command.

```sh
watcher watch ./my/config.js
```

## Configuration

All of the configuration is done in a js file. Below is a basic example.

```js
module.exports = {
    watchPaths: ['src/**/*.ts'],
    watchers: [
        {
            test: /\.ts$/,
            command: './node_modules/.bin/tsc'
        }
    ]
};
```

### Root Configuration

#### watchPaths

Watch paths is an array of path globs that will look for files to watch.

```js
watchPaths: ['src/**/*.ts'],
```

#### ignorePaths

Ignore paths is an array of path that watcher will not watch for files. Each
item can be a glob or regex

```js
ignorePaths: [/node_modules/],
```

#### watchers

An array of watcher objects that will define the command to run when a file has
changed.

### Watch Configuration

#### test

Test is a regex that the file path will be tested against to see if the command
should be run.

```js
test: /\.ts$/,
```

#### command

The command that will be executed with `child_process`. The command can have
a `{file}` place holder in that will be replaced with the file path of the
changed file.

```js
command: './node_modules/.bin/jest {file}',
```

#### stdout

This is a boolean value so you can stop watcher from printing the output of the
command. This is true by default.

Note: All `stderr` is printed to the terminal

```js
stdout: false,
```

#### getOutput

This is a callback function so you can manipulate the output to the console.
This is best used when you turn off `stdout` and you can print your own
message for logging.

```js
getOutput(exitCode, output, file) {
    const success = exitCode > 0 ? 'Error' : 'Success';
    return `${success}: ${file}`;
}
```

#### getFile

This is a callback function so you can change the file that will be replaced
in the `command`. Below is an example of manipulating the file so it runs the
test associated to the file being saved.

```js
const path = require('path');
getFile(file) {
    if (/\.test\.ts$/.test(file)) {
        return file;
    }

    const parsed = path.parse(file);
    return `${parsed.dir}/${parsed.name}.test.ts`;
}
```
