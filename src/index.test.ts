import * as Watcher from './index';
import chokidar from 'chokidar';

let watcher: chokidar.FSWatcher | null = null;

console.log = jest.fn();

const sleep = (delay: number): Promise<void> => {
    return new Promise(resolve => setTimeout(() => resolve(), delay));
};

afterEach(() => {
    if (watcher !== null) {
        watcher.close();
    }
});

test('Get the config', async () => {
    const config = await Watcher.getConfig();
    expect(config).toHaveProperty('watchPaths');
    expect(config).toHaveProperty('watchers');
});

test('Get look for custom config', async () => {
    try {
        await Watcher.getConfig('not-a-config.js');
    } catch (e) {
        expect(e.message).toBe('File "not-a-config.js" is not found');
        return true;
    }

    fail('Exception should have been thrown');
});

test('Pass a directory to as a config file', async () => {
    try {
        await Watcher.getConfig('src');
    } catch (e) {
        expect(e.message).toBe('File "src" is not found');
        return true;
    }

    fail('Exception should have been thrown');
});

test('Running a command', async () => {
    Watcher.runCommand('src/commands', {
        test: /any/,
        command: 'ls {file}',
        stdout: false,
        getOutput: (code, output) => output
    });

    await sleep(500);
    expect(console.log).toBeCalledTimes(1);
    expect(console.log).toBeCalledWith('watch.ts\n');
});

test('Manipulate output', async () => {
    Watcher.runCommand('src/commands', {
        test: /any/,
        command: 'ls {file}',
        stdout: false,
        getOutput: () => 'this is the output'
    });

    await sleep(500);
    expect(console.log).toBeCalledTimes(1);
    expect(console.log).toBeCalledWith('this is the output');
});

test('Create watcher successfully', async () => {
    watcher = Watcher.createWatcher({
        watchPaths: ['src/commands/*.ts'],
        watchers: [
            {
                test: /\.ts$/,
                command: './node_modules/.bin/tsc'
            }
        ]
    });

    await sleep(1000);
    expect(watcher.listenerCount('change')).toBe(1);
    expect(console.log).toBeCalledTimes(1);
    expect(console.log).toBeCalledWith('Watching 1 files for changes ...');
});
