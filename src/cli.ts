import {error} from './output';

import program = require('commander');

const packageJson: any = require('../package.json');

/**
 * Setup the cli app
 */
program.name(packageJson.name);
program.version(packageJson.version);
program.description(packageJson.description);

/**
 * Define all of the tasks as commands
 */
require('./commands/watch');

/**
 * Handel unknown commands
 */
program.on('command:*', function() {
    error(
        new Error(
            `Invalid command: ${program.args.join(
                ' '
            )}\nSee --help for a list of available commands.`
        )
    );
});

/**
 * Run the cli app
 */
program.parse(process.argv);

/**
 * Show help if no arguments are passed
 */
if (!program.args.length) program.help();
