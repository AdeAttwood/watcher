import * as Watcher from '../index';
import program = require('commander');
import {error} from '../output';

const command = program.command('watch');
command.description('Watch files for changes biased on a config');

command.action(async configFile => {
    try {
        const config = await Watcher.getConfig(
            typeof configFile === 'string' ? configFile : undefined
        );

        Watcher.createWatcher(config);
    } catch (e) {
        error(e);
    }
});
