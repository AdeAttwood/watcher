export interface ExecException extends Error {
    code?: number;
}

export const error = (error: ExecException): void => {
    console.error(error.message);
    process.exit(error.code || 1);
};
