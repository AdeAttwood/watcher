import {spawn} from 'child_process';
import {lstatSync} from 'fs';
import * as chokidar from 'chokidar';

export type StringOrRegex = string | RegExp;

export interface Watcher {
    test: RegExp;
    command: string;
    stdout?: boolean;
    getOutput?: (exitCode: number, output: string, file: string) => string;
    getFile?: (file: string) => string;
}

export interface WatcherConfig {
    watchPaths: string[];
    ignorePaths?: StringOrRegex[];
    watchers: Watcher[];
}

export const runCommand = (file: string, watcher: Watcher): void => {
    if (!file) {
        return;
    }

    const command = watcher.command.replace('{file}', file).split(' ');
    const testProcess = spawn(command[0], command.slice(1, command.length));
    testProcess.stderr.pipe(process.stderr);

    if (watcher.stdout !== false) {
        testProcess.stdout.pipe(process.stdout);
    }

    let output = '';
    testProcess.stdout.on('data', buffer => {
        output += buffer.toString();
    });

    testProcess.on('close', code => {
        if (watcher.getOutput) {
            console.log(watcher.getOutput(code, output, file));
        }

        output = '';
    });

    testProcess.on('error', err => console.error(err));
};

export const createWatcher = (config: WatcherConfig): chokidar.FSWatcher => {
    let watchCount = 0;

    const watcher = chokidar.watch(config.watchPaths, {
        ignored: config.ignorePaths,
        persistent: true
    });

    watcher.on('change', file => {
        for (const watcher of config.watchers) {
            if (watcher.test.test(file)) {
                if (watcher.getFile) {
                    file = watcher.getFile(file);
                }
                runCommand(file, watcher);
            }
        }
    });

    watcher.on('add', () => {
        watchCount++;
    });

    watcher.on('ready', () => {
        console.log(`Watching ${watchCount} files for changes ...`);
    });

    watcher.on('error', err => console.error(err));

    return watcher;
};

export const getConfig = async (file?: string): Promise<WatcherConfig> => {
    const configFile =
        typeof file !== 'undefined'
            ? file
            : process.cwd() + '/watcher.config.js';
    try {
        if (!lstatSync(configFile).isFile()) {
            throw new Error(`File "${file}" is not a valid file`);
        }

        return require(configFile);
    } catch (e) {
        throw new Error(`File "${configFile}" is not found`);
    }
};
